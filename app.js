var express = require("express");
var bodyParser = require('body-parser');
var port = process.env.PORT || 3000;

var app = express();
app.use(bodyParser.json());

// Data Mocks
var usersFile = require('./users.json');
var accountsFile = require('./accounts.json');

// Constants
const URI = '/api-uruguay/v1/'

// Init Server
app.listen(port);
console.log('Escuchando en el puerto 3000');

// GET Users
app.get(URI + 'users', function(req, res) {
  console.log('GET /users');
  res.send(usersFile);
})

// GET User by ID
app.get(URI + 'users/:id',function(req, res) {
  let idUser = req.params.id;
  console.log('GET /users/' + idUser);
  res.send(usersFile[idUser -1]);
})

// GET Accounts
app.get(URI + 'accounts', function(req, res) {
  console.log('GET /accounts');
  res.send(accountsFile);
})

// POST users
app.post(URI + 'users', function(req, res) {
    console.log('POST users');

    let lastIndex = usersFile.length;
    var newUser = req.body;
    newUser.id = lastIndex + 1;

    var newUser = {
      'id' : lastIndex + 1,
      'first_name' : req.body.first_name,
      'last_name' : req.body.last_name,
      'email' : req.body.email,
      'password' : req.body.password
    };

    usersFile.push(newUser);
    res.send({'message': 'POST realizado correctamente', newUser});
  })


  // POST users
  app.post(URI + 'users_header', function(req, res) {
      console.log('POST users from headers');

      let lastIndex = usersFile.length;
      var newUser = {
        'id' : lastIndex + 1,
        'first_name' : req.headers.first_name,
        'last_name' : req.headers.last_name,
        'email' : req.headers.email,
        'password' : req.headers.password
      };
      usersFile.push(newUser);

      res.send({'message': 'POST realizado correctamente', newUser});
  })

  //PUT user
  app.put(URI + 'users/:id', function(req, res){
   console.log('PUT users');
   let idUpdate = req.params.id;
   let existe = true;
   if(usersFile[idUpdate-1] == undefined){
     console.log('Usuario NO existe');
     existe = false;
   }
   else {
     var userNuevo = {
       'id': idUpdate,
       'first_name': req.body.first_name,
       'last_name': req.body.last_name,
       'email': req.body.email,
       'password': req.body.password
     };
     usersFile[idUpdate - 1] = userNuevo;
   }
   var mensRespone = existe ? {'msg':'update ok'} : {'msg':'failed'};
   var statusCode = existe ? 200 : 400;
   res.status(statusCode).send(mensRespone);
  });

  // DELETE user
  app.delete(URI + 'users/:id', function(req, res){
     console.log('DELETE user');
     let idDelete = req.params.id;
     let existe = true;
     if(usersFile[idDelete-1] == undefined){
       console.log('Usuario NO existe');
       existe = false;
     }
     else {
       usersFile.splice(idDelete - 1, 1);
     }

     var mensRespone = existe ? {'msg':'delete ok'} : {'msg':'delete failed'};
     var statusCode = existe ? 200 : 400;
     res.status(statusCode).send(mensRespone);
   });


   // GET con query string
   app.get (URI +"qusers", function (req,res) {
         console.log('GET con query string');
         let qname= req.query.qname;
         let lname= req.query.lname;
         console.log(qname);
         console.log(lname);
         var usuario = usersFile.filter(user=>user.first_name==qname && user.last_name==lname);
         if(usuario.length>0){
           res.send({'msg': 'usuario encontrado',usuario});
         }else{
             res.send({'msg': 'usuario NO encontrado'});
         }
       }
   );
